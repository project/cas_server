<?php

namespace Drupal\cas_server\Exception;

/**
 * Exception thrown when wrong ticket type has been provided in a request.
 */
class TicketTypeException extends \Exception {}
