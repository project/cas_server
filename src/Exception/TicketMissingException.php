<?php

namespace Drupal\cas_server\Exception;

/**
 * Exception thrown when ticket is missing from request.
 */
class TicketMissingException extends \Exception {}
