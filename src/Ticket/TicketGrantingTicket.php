<?php

namespace Drupal\cas_server\Ticket;

/**
 * Ticket used in CAS protocol to grant a SSO ticket.
 */
class TicketGrantingTicket extends Ticket {}
